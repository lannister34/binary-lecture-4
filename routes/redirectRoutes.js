import {Router} from 'express';

const router = Router();

router.get('/', (req, res) => {

  if (req.query.message) {
    req.flash('error_message', req.query.message);
  }

  if (req.query.redirect) {
    res.redirect(req.query.redirect);
  } else {
    res.redirect('login');
  }

});

export default router;
