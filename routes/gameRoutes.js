import {Router} from 'express';
import {texts} from '../data.js';

const router = Router();

router.get('/', (req, res) => {
  res.render('game');
});

router.get('/texts/:id', (req, res) => {
  if (texts[req.params.id]) {
    res.status(200).send({
      'text': Buffer.from(texts[req.params.id]).toString('base64'),
    });
  } else {
    res.status(404).send({
      'error': true,
    });
  }
});

export default router;
