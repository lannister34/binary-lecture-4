export const texts = [
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
  '1234567890 1234567890 1234567890 1234567890 1234567890',
];

export const botPhrases = {
  'greeting': [
    'Hello, my name is Yaroslav Kuznetsov, I will comment on this race.',
    'Good afternoon, my nick is NS, today we are waiting for an interesting race.',
  ],
  'join': [
    '$0 join the race. We are waiting for an interesting confrontation.',
    '$0, welcome to this race. I hope you\'re ready for some serious competition.',
    'Meanwhile, we have a new racer. Welcome, $0.',
  ],
  'leave': [
    '$0 left the race.',
    'Unfortunately, the race will be without $0.'
  ],
  'start': [
    'All riders are in position. Today in the race participate: $all.'
  ],
  'periodic': [
      'At the moment, $0 is in the lead, followed by $1<% if $2 %>, and $2 closes the top three<% endif %>.'
  ],
  'random': [
      'It should rain in the next hour. Let\'s see how the racers cope with the wet track.',
      'News from Formula 1: Valtteri Bottas and Lewis Hamilton of the Mercedes team lead the Championship.',
      'This race reminds me of the final of The International 3. So cool!'
  ],
  'beforeFinish': [
    '$0 is coming to the finish line, but $1<% if $2 %> and $2 are<% else %> is<% endif %> on his heels!',
    '$0 is close to victory, but everything is not yet decided...',
  ],
  'finish': [
    '$0 crossed the finish line.',
    '$0 finished the race.'
  ],
  'end': [
      '$0 becomes the winner of the race.<% if $1 %> $1 took the second place.<% if $2 %>$2 took the third place.<% endif %><% endif %>'
  ]
};

export default {texts};
