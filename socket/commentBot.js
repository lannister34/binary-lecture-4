import {botPhrases} from '../data';

class CommentController {
  constructor() {

  };

  createPhrase(event, data = []) {
    let phrase = this.getRandomPhrase(event);
    const indexRegexp = new RegExp(/\$\d+/g);
    const allRegexp = new RegExp(/\$all/g);
    phrase = this.parseStringConditions(phrase, data);

    allRegexp.test(phrase) && (
        phrase = phrase.replace(allRegexp, data.join(', '))
    );

    const matchIndexRegexp = phrase.match(indexRegexp);

    if (matchIndexRegexp) {
      matchIndexRegexp.forEach((match) => {
        const variable = Number(match.substr(1));
        phrase = phrase.replace(match, data[variable]);
      });
    }

    return phrase;
  }

  parseStringConditions(string, data = []) {
    const ifRegexp = new RegExp(/<% if \$\d+ %>.*<% endif %>/g);

    while (string.match(ifRegexp)) {
      const matchRegexp = string.match(ifRegexp);

      matchRegexp.forEach((condition) => {

        const variable = Number(condition.split(' ')[2].substr(1));
        const isCondition = Boolean(data[variable]);

        let actions = condition.split('<% else %>').
            map((cond) => cond.replace(/<% if \$\d+ %>/, '').
                replace(/<% endif %>/, ''));
        let ifConditionTrue = actions[0];
        let ifConditionFalse = actions[1] ? actions[1] : '';

        string = string.replace(condition,
            isCondition ? ifConditionTrue : ifConditionFalse);
      });
    }

    return string;
  }

  getRandomPhrase(event) {
    const phrasesArray = botPhrases[event];

    if (!phrasesArray) {
      return null;
    }

    const phraseIndex = this.getPhraseIndex(phrasesArray.length);

    return phrasesArray[phraseIndex];
  }

  getPhraseIndex(length) {
    return this.getRandomInt(0, length);
  }

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

}

class CommentBot extends CommentController {
  constructor() {
    super();
  }

  greeting() {
    return this.createPhrase('greeting');
  }

  join(users) {
    return this.createPhrase('join', users);
  }

  leave(users) {
    return this.createPhrase('leave', users);
  }

  start(users) {
    return this.createPhrase('start', users);
  }

  periodic(users) {
    return this.createPhrase('periodic', users);
  }

  random() {
    return this.createPhrase('random');
  }

  beforeFinish(users) {
    return this.createPhrase('beforeFinish', users);
  }

  finish(users) {
    return this.createPhrase('finish', users);
  }

  end(users) {
    return this.createPhrase('end', users);
  }
}

export const commentBot = new CommentBot();