import {createElement, append} from './domHelper.mjs';

export function createRoomCard({roomName, currentUser, id, membersCount, action}) {
  const wrapper = createElement({
    'tagName': 'div',
    'className': 'col-sm-3',
  });

  const card = createElement({
    'tagName': 'div',
    'className': 'card',
  });

  const cardBody = createElement({
    'tagName': 'div',
    'className': 'card-body',
  });

  const cardOnline = createElement({
    'tagName': 'div',
    'className': 'card-online',
    'textContent': membersCount + (membersCount === 1 ? ' user' : ' users') +
        ' connected',
  });

  const cardTitle = createElement({
    'tagName': 'div',
    'className': 'card-title',
    'textContent': roomName,
  });

  const cardButton = createElement({
    'tagName': 'button',
    'className': 'btn btn-primary',
    'attributes': {
      'data-id': id,
    },
    'listeners': {
      'click': action && action.bind(this, roomName, currentUser),
    },
    'textContent': 'Войти',
  });

  append(cardOnline, cardBody);
  append(cardTitle, cardOnline, 'after');
  append(cardButton, cardTitle, 'after');
  append(cardBody, card);
  append(card, wrapper);

  return wrapper;
}

export function createRoomDOM({roomName, currentUser, status, members, closeAction, playAction}) {
  const wrapper = createElement({
    'tagName': 'div',
    'className': 'room-wrapper',
  });

  const mainContainer = createElement({
    'tagName': 'div',
    'className': 'main-container',
  });

  const mainRow = createElement({
    'tagName': 'div',
    'className': 'row main-row',
  });

  const infoWrapper = createElement({
    'tagName': 'div',
    'className': 'col-md-4 info-wrapper',
  });

  const titleBlock = createElement({
    'tagName': 'div',
    'className': 'title-block',
  });

  const title = createElement({
    'tagName': 'h1',
    'className': 'room-title',
    'textContent': roomName,
  });

  const backButton = createElement({
    'tagName': 'button',
    'className': 'back-button btn btn-secondary',
    'textContent': 'Back to Rooms',
    'attributes': {
      'type': 'button',
    },
    'listeners': {
      'click': closeAction &&
          closeAction.bind(null, roomName, currentUser.name),
    },
  });

  const userList = createElement({
    'tagName': 'div',
    'className': 'row',
  });

  const gameWrapper = createElement({
    'tagName': 'div',
    'className': 'col-md-5 game-wrapper',
  });

  const gameArea = createElement({
    'tagName': 'div',
    'className': 'game-area',
  });

  const playButton = createElement({
    'tagName': 'button',
    'className': 'btn play-button ' +
        (status ? 'btn-secondary' : 'btn-success'),
    'textContent': (status ? 'NOT READY' : 'READY'),
    'listeners': {
      'click': playAction.bind(null, roomName, (!status)),
    },
  });

  members.forEach((user) => {
    const userBlock = createElement({
      'tagName': 'div',
      'className': 'user-block col-12' +
          (user.name === currentUser ? ' current-user' : ''),
      'attributes': {
        'data-name': user.name,
      },
    });

    const userStatus = createElement({
      'tagName': 'div',
      'className': 'user-status ' + (user.status ? 'status-on' : 'status-off'),
    });

    const userName = createElement({
      'tagName': 'h3',
      'className': 'user-name',
      'textContent': user.name + (user.name === currentUser ? ' (you)' : ''),
    });

    const progressBar = createElement({
      'tagName': 'div',
      'className': 'progress-bar',
    });

    const progress = createElement({
      'tagName': 'div',
      'className': 'progress',
    });

    append(userStatus, userBlock);
    append(userName, userBlock);
    append(progress, progressBar);
    append(progressBar, userBlock);
    append(userBlock, userList);
  });

  const commentWrapper = createElement({
    'tagName': 'div',
    'className': 'col-md-3 comment-wrapper',
  });

  const commentatorBlock = createElement({
    'tagName': 'div',
    'className': 'comment-block',
  });

  const textCloudBlock = createElement({
    'tagName': 'div',
    'className': 'text-cloud-block',
  });

  const textCloud = createElement({
    'tagName': 'div',
    'className': 'text-cloud',
  });

  const textCloudContent = createElement({
    'tagName': 'span',
    'className': 'text-cloud-content',
  });

  const commentator = createElement({
    'tagName': 'img',
    'className': 'commentator',
    'attributes': {
      'src': '/images/commentator.png',
    },
  });

  append(title, titleBlock);
  append(backButton, titleBlock);
  append(titleBlock, infoWrapper);
  append(userList, infoWrapper);
  append(infoWrapper, mainRow);

  append(playButton, gameArea);
  append(gameArea, gameWrapper);
  append(gameWrapper, mainRow);

  append(commentator, commentatorBlock);
  append(textCloudContent, textCloud);
  append(textCloud, textCloudBlock);
  append(textCloudBlock, commentatorBlock);
  append(commentatorBlock, commentWrapper);
  append(commentWrapper, mainRow);

  append(mainRow, mainContainer);
  append(mainContainer, wrapper);
  append(wrapper);

  return wrapper;
}
