export function createElement({tagName = 'div', className, resize, textContent, listeners = {}, attributes = {}}) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  if (textContent) {
    element.innerHTML = textContent;
  }

  Object.keys(attributes).
      forEach((key) => element.setAttribute(key, attributes[key]));

  Object.keys(listeners).forEach((event) => {
    typeof listeners[event] === 'function' &&
    element.addEventListener(event, listeners[event], false);
  });

  return element;
}

export function append(element, target = document.body, place) {
  switch (place) {
    case 'before':
      target.insertAdjacentElement('beforebegin', element);
      break;
    case 'after':
      target.insertAdjacentElement('afterend', element);
      break;
    case 'firstChild':
      target.insertAdjacentElement('afterbegin', element);
      break;
    case 'lastChild':
      target.insertAdjacentElement('beforeend', element);
      break;
    default:
      target.insertAdjacentElement('beforeend', element);
      break;
  }
}

export function deleteAllChildren(node) {
  node.innerHTML = '';
}