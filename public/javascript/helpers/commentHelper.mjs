export class CommentBot {
  constructor() {

  }

  initRoot(root) {
    this.root = root;
    this.commentBlockNode = this.root.querySelector('.text-cloud-block');
    this.commentContentNode = this.commentBlockNode.querySelector(
        '.text-cloud-content');
    if (this.content) {
      this.commentContentNode.innerHTML = this.content;
      this.showCommentBlock(true);
    }
  }

  updateContent(content) {
    this.hideCommentBlock();
    this.content = content;
    this.commentContentNode.innerHTML = content;
    this.showCommentBlock();
  }

  hideCommentBlock() {
    this.commentBlockNode.style.transition = 'unset';
    this.commentBlockNode.style.transform = 'scale(0)';
  }

  showCommentBlock(instant) {
    !instant && (this.commentBlockNode.style.transition = 'transform 1s');
    requestAnimationFrame(() => {
      this.commentBlockNode.style.transform = 'scale(1)';
    });
  }
}